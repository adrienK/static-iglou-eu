package main

import (
	"bufio"
	"bytes"
	"os"
	"text/template"
	"time"

	"github.com/tdewolff/minify/v2"
	mcss "github.com/tdewolff/minify/v2/css"
	mhtml "github.com/tdewolff/minify/v2/html"
)

const (
	HtmlSrcFile = "../src/DiscoveryOne.html"
	HtmlDstFile = "../public/DiscoveryOne.html"

	CssSrcFile = "../src/css/Monolith.css"
	CssDstFile = "../public/css/Monolith.css"

	DATASrcFile = "../data/data.stf"
)

func main() {
	html, err := os.ReadFile(HtmlSrcFile)
	if err != nil {
		panic(err)
	}

	css, err := os.ReadFile(CssSrcFile)
	if err != nil {
		panic(err)
	}

	data, err := parseData(DATASrcFile)
	if err != nil {
		panic(err)
	}

	if err = buildHTML(&html, &data); err != nil {
		panic(err)
	}

	if err = minifyStuff(&html, &css); err != nil {
		panic(err)
	}

	err = os.WriteFile(HtmlDstFile, html, 0644)
	if err != nil {
		panic(err)
	}

	err = os.WriteFile(CssDstFile, css, 0644)
	if err != nil {
		panic(err)
	}
}

func parseData(file string) (data []string, err error) {
	buff, err := os.Open(file)
	if err != nil {
		return
	}
	defer buff.Close()

	scanner := bufio.NewScanner(buff)
	scanner.Split(bufio.ScanLines)

	// first line is the build date for the last-modified meta tag
	data = append(data, time.Now().Format("2006-01-02"))

	for scanner.Scan() {
		data = append(data, scanner.Text())
	}

	return
}

func buildHTML(html *[]byte, data *[]string) (err error) {
	tpl := template.Must(template.New("").Parse(string(*html)))

	buff := new(bytes.Buffer)
	if err = tpl.Execute(buff, *data); err != nil {
		return
	}

	*html = buff.Bytes()

	return
}

func minifyStuff(html *[]byte, css *[]byte) (err error) {
	m := minify.New()
	// m.AddFunc("text/css", mcss.Minify)
	m.Add("text/css", &mcss.Minifier{})

	// m.AddFunc("text/html", mhtml.Minify)
	m.Add("text/html", &mhtml.Minifier{
		KeepEndTags:      true,
		KeepDocumentTags: true,
	})

	*html, err = m.Bytes("text/html", *html)
	if err != nil {
		return
	}

	*css, err = m.Bytes("text/css", *css)
	if err != nil {
		return
	}

	return
}
